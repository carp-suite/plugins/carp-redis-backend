package main

import (
	"log"
	"net"
	"os"

	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/core"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

const (
	protocol = "unix"
	sockAddr = "/run/carp/ccb.sock"
)

func main() {
	plugin := core.NewCacheBackendPlugin()

	// port := 8080
	// lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	// if err != nil {
	// 	log.Fatalf("failed to listen: %v", err)
	// }
	if _, err := os.Stat(sockAddr); !os.IsNotExist(err) {
		if err := os.RemoveAll(sockAddr); err != nil {
			log.Fatal(err)
		}
	}

	listener, err := net.Listen(protocol, sockAddr)
	if err != nil {
		log.Fatal(err)
	}

	gs := carpdriver.InitGrpc(plugin)

	log.Println("Starting server")
	gs.Serve(listener)
}
