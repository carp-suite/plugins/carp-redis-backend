FROM golang:1.19.0-alpine3.15
WORKDIR /carp-cache-backend
RUN apk update && apk upgrade
RUN apk add --no-cache gcc musl-dev redis
COPY . .
RUN go build -o main ./cmd/main/main.go
CMD ["./main"]
