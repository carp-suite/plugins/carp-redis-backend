package internal_test

import (
	"context"
	"log"
	"net"
	"testing"

	"github.com/cucumber/godog"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/core"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/db"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/helpers"
	"gitlab.com/carp-suite/plugins/carpdriver"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

type pluginTest struct {
	plugin     *core.CacheBackendPlugin
	listener   *bufconn.Listener
	ctx        context.Context
	client     pb.PluginClient
	grpcServer *grpc.Server
	result     interface{}
}

type cacheEntry struct {
	ID    uuid.UUID
	Bytes []byte
}

func TestBackendCachePlugin(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./plugin.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	pt := new(pluginTest)

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := pt.setup(sc)
		return ctx, err
	})

	sc.Step(`^the next cache:$`, pt.theNextCache)
	sc.Step(`^cache end$`, pt.cacheEnd)
	sc.Step(`^cache start (.+)$`, pt.cacheStart)
	sc.Step(`^I should get the next cache:$`, pt.shouldGetTheNextCache)
}

func (pt *pluginTest) setup(sc *godog.Scenario) error {
	pt.plugin = core.NewCacheBackendPlugin()

	database := db.NewClient("localhost", 6379, 0, "", "")
	pt.plugin.Database = database

	bufSize := 1024 * 1024
	pt.listener = bufconn.Listen(bufSize)
	gs := carpdriver.InitGrpc(pt.plugin)
	go func() {
		if err := gs.Serve(pt.listener); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()

	if pt.client != nil {
		return nil
	}

	pt.ctx = context.Background()
	conn, err := grpc.DialContext(pt.ctx, "bufnet", grpc.WithContextDialer(pt.dialer), grpc.WithInsecure())
	if err != nil {
		return err
	}

	pt.client = pb.NewPluginClient(conn)

	return nil
}

func (pt *pluginTest) dialer(context.Context, string) (net.Conn, error) {
	return pt.listener.Dial()
}

func (pt *pluginTest) theNextCache(table *godog.Table) error {
	assist := assistdog.NewDefault()
	type cache struct {
		ID    string
		Bytes string
	}

	result, err := assist.CreateSlice(new(cache), table)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	caches := result.([]*cache)
	cachearr := make([]*cacheEntry, 0)

	for _, c := range caches {

		id, err := uuid.Parse(c.ID)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		bytes := []byte(c.Bytes)

		cachearr = append(cachearr, &cacheEntry{
			ID:    id,
			Bytes: bytes,
		})
	}

	pt.result = cachearr

	return nil
}

func (pt *pluginTest) cacheEnd() error {
	caches := pt.result.([]*cacheEntry)
	streamCacheEnd, err := pt.client.CacheEnd(pt.ctx)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	for _, c := range caches {
		cacheRes := &pb.CacheResponse{
			Id:           c.ID[:],
			ResponseByte: c.Bytes,
		}

		if err := streamCacheEnd.Send(cacheRes); err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

	}

	streamCacheEnd.CloseSend()

	_, err = streamCacheEnd.Recv()
	if err != nil {
		return (helpers.AssertActual(assert.NoError, err, err.Error()))
	}

	return nil
}

func (pt *pluginTest) cacheStart(id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	streamCacheStart, err := pt.client.CacheStart(pt.ctx)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	cacheReq := &pb.CacheRequest{
		Id: uid[:],
	}

	if err := streamCacheStart.Send(cacheReq); err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	streamCacheStart.CloseSend()

	cacheRes, err := streamCacheStart.Recv()
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	res := cacheRes.GetResponse()
	if res != nil {
		resId, err := uuid.FromBytes(res.Id)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		contentChan := make(chan []byte)
		go func() {
			contentChan <- res.ResponseByte
		}()

		pt.result = &carpdriver.Cache{
			ID:    resId,
			Bytes: contentChan,
		}
	}

	return nil
}

func (pt *pluginTest) shouldGetTheNextCache(instance *godog.Table) error {
	assist := assistdog.NewDefault()
	type cache struct {
		ID    string
		Bytes string
	}

	result, err := assist.CreateInstance(new(cache), instance)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	expected := result.(*cache)
	obtained := pt.result.(*carpdriver.Cache)

	err = helpers.AssertExpectedActual(
		assert.Equal, expected.ID, obtained.ID.String(),
		"expected ID %s, obtained %s", expected.ID, obtained.ID.String(),
	)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	obtainedBytes := <-obtained.Bytes

	return helpers.AssertExpectedActual(
		assert.Equal, []byte(expected.Bytes), obtainedBytes,
		"expected Bytes %s, obtained %s", []byte(expected.Bytes), obtainedBytes,
	)
}
