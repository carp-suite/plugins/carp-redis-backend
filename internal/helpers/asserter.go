package helpers

import (
	"fmt"

	"github.com/kinbiko/jsonassert"
	"github.com/stretchr/testify/assert"
)

type asserter struct {
	err error
}

func (a *asserter) Errorf(format string, args ...interface{}) {
	a.err = fmt.Errorf(format, args...)
}

type (
	actualAssertion[T interface{}] func(t assert.TestingT, actual T, msgAndArgs ...interface{}) bool
	expectedActualAssertion        func(t assert.TestingT, expected, actual interface{}, msgAndArgs ...interface{}) bool
)

func AssertActual[T interface{}](a actualAssertion[T], actual T, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, actual, msgAndArgs...)
	return t.err
}

func AssertExpectedActual(a expectedActualAssertion, expected, actual interface{}, msgAndArgs ...interface{}) error {
	var t asserter
	a(&t, expected, actual, msgAndArgs...)
	return t.err
}

func AssertExpectedActualJson(expected, actual string, fmtArgs ...interface{}) error {
	var t asserter
	ja := jsonassert.New(&t)
	ja.Assertf(expected, actual, fmtArgs...)
	return t.err
}
