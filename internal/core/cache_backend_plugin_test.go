package core_test

import (
	"context"
	"testing"

	"github.com/cucumber/godog"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/core"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/db"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/helpers"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

type backendCachePluginTest struct {
	plugin *core.CacheBackendPlugin
	result interface{}
}

type cacheEntry struct {
	ID    uuid.UUID
	Bytes []byte
}

func TestBackendCachePlugin(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./cache_backend_plugin.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	pt := new(backendCachePluginTest)

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := pt.setup(sc)
		return ctx, err
	})

	sc.Step(`^in the database the next caches:$`, pt.databaseTheNextCaches)
	sc.Step(`^the next cache:$`, pt.theNextCache)
	sc.Step(`^cache start (.+)$`, pt.cacheStart)
	sc.Step(`^cache end$`, pt.cacheEnd)
	sc.Step(`^I should get the next cache:$`, pt.shouldGetTheNextCache)
	sc.Step(`^the database should have the next cache:$`, pt.shouldHaveTheNextCache)
}

func (pt *backendCachePluginTest) setup(sc *godog.Scenario) error {
	pt.plugin = core.NewCacheBackendPlugin()

	database := db.NewClient("localhost", 6379, 0, "", "")
	pt.plugin.Database = database

	return nil
}

func (pt *backendCachePluginTest) databaseTheNextCaches(table *godog.Table) error {
	assist := assistdog.NewDefault()
	type cache struct {
		ID    string
		Bytes string
	}

	result, err := assist.CreateSlice(new(cache), table)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	caches := result.([]*cache)
	for _, c := range caches {
		err := pt.plugin.Database.Set(*pt.plugin.Ctx, c.ID, c.Bytes, 0).Err()
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}
	}

	return nil
}

func (pt *backendCachePluginTest) theNextCache(table *godog.Table) error {
	assist := assistdog.NewDefault()
	type cache struct {
		ID    string
		Bytes string
	}

	result, err := assist.CreateSlice(new(cache), table)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	caches := result.([]*cache)
	cachearr := make([]*cacheEntry, 0)

	for _, c := range caches {

		id, err := uuid.Parse(c.ID)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		bytes := []byte(c.Bytes)

		cachearr = append(cachearr, &cacheEntry{
			ID:    id,
			Bytes: bytes,
		})
	}

	cachearr = append(cachearr, &cacheEntry{Bytes: nil})
	pt.result = cachearr

	return nil
}

func (pt *backendCachePluginTest) cacheStart(id string) error {
	uid, err := uuid.Parse(id)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	c := &carpdriver.Cache{
		ID: uid,
	}

	cache, err := pt.plugin.CacheStart(c)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	pt.result = cache
	return nil
}

func (pt *backendCachePluginTest) cacheEnd() error {
	cachearr := pt.result.([]*cacheEntry)
	first := true
	contentChan := make(chan []byte)
	errChan := make(chan error)
	contentErrChan := make(chan error)
	quitChan := make(chan int)

	for _, cache := range cachearr {
		if first {
			go func() {
				err := pt.plugin.CacheEnd(&carpdriver.Cache{
					ID:    cache.ID,
					Bytes: contentChan,
					Err:   contentErrChan,
				})
				if err != nil {
					errChan <- helpers.AssertActual(assert.NoError, err, err.Error())
				}
				quitChan <- 0
			}()
			first = false
		}

		contentChan <- cache.Bytes
	}

	select {
	case <-quitChan:
		return nil
	case err := <-errChan:
		return err
	}
}

func (pt *backendCachePluginTest) shouldGetTheNextCache(instance *godog.Table) error {
	assist := assistdog.NewDefault()
	type cache struct {
		ID    string
		Bytes string
	}

	result, err := assist.CreateInstance(new(cache), instance)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	expected := result.(*cache)
	actual := pt.result.(*carpdriver.Cache)

	return helpers.AssertExpectedActual(
		assert.Equal, []byte(expected.Bytes), <-actual.Bytes,
		"Expected: %v\nActual: %v", []byte(expected.Bytes), <-actual.Bytes,
	)
}

func (pt *backendCachePluginTest) shouldHaveTheNextCache(table *godog.Table) error {
	assist := assistdog.NewDefault()
	type cache struct {
		ID    string
		Bytes string
	}

	result, err := assist.CreateSlice(new(cache), table)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	expected := result.([]*cache)
	actual, err := pt.plugin.Database.Get(*pt.plugin.Ctx, expected[0].ID).Result()
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	return helpers.AssertExpectedActual(
		assert.Equal, expected[0].Bytes, actual,
		"Expected: %v\nActual: %v", expected[0].Bytes, actual,
	)
}
