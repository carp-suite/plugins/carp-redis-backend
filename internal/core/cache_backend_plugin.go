package core

import (
	"context"
	"log"

	"github.com/buger/jsonparser"
	"github.com/redis/go-redis/v9"
	"gitlab.com/carp-suite/plugins/carp-cache-backend/internal/db"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

type CacheBackendPlugin struct {
	Database *redis.Client
	Ctx      *context.Context
}

func NewCacheBackendPlugin() *CacheBackendPlugin {
	context := context.Background()
	p := &CacheBackendPlugin{
		Ctx: &context,
	}

	return p
}

func (p *CacheBackendPlugin) Configure(ctx []byte) error {
	host, _ := jsonparser.GetString(ctx, "host")
	port, _ := jsonparser.GetInt(ctx, "port")
	database, _ := jsonparser.GetInt(ctx, "database")
	username, _ := jsonparser.GetString(ctx, "username")
	password, _ := jsonparser.GetString(ctx, "password")

	log.Println("Configure on plugin: ", host, port, database, username, password)

	p.Database = db.NewClient(host, int(port), int(database), username, password)
	return nil
}

func (p *CacheBackendPlugin) CacheStart(c *carpdriver.Cache) (*carpdriver.Cache, error) {
	log.Println("CacheStart on plugin: ", c.ID)
	contentChan := make(chan []byte)
	errChan := make(chan error)

	go func() {
		content, err := p.Database.Get(*p.Ctx, c.ID.String()).Bytes()
		log.Println("CacheStart on plugin get")
		if err != nil {
			errChan <- err
			return
		}

		contentChan <- content
		contentChan <- nil
	}()

	return &carpdriver.Cache{
		ID:    c.ID,
		Bytes: contentChan,
		Err:   errChan,
	}, nil
}

func (p *CacheBackendPlugin) CacheEnd(c *carpdriver.Cache) error {
	buffer := make([]byte, 0)

	for bytes := range c.Bytes {
		if bytes == nil {
			break
		}
		buffer = append(buffer, bytes...)
	}

	err := p.Database.Set(*p.Ctx, c.ID.String(), buffer, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (p *CacheBackendPlugin) RequestStart(req *carpdriver.Request) (*carpdriver.Request, *carpdriver.Response, error) {
	return nil, nil, nil
}

func (p *CacheBackendPlugin) RequestEnd(res *carpdriver.Response) (*carpdriver.Response, error) {
	return nil, nil
}
