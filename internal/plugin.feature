Feature: Cache Backend Plugin
  The plugin will listen to cache start events, modifying the 
  response to be returned to the client, using a Redis database 
  instance as the storage method

  Scenario: Cache Start and Cache End Flow
    Given the next cache:
      | ID                                   | Bytes         |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | response      |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | Cached        |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | OnBytes       |
    When cache end 
    And cache start e38a9003-bc2d-4ba3-ac5d-d6a8c74de162
    Then I should get the next cache:
      | ID    | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 |
      | Bytes | responseCachedOnBytes                |
