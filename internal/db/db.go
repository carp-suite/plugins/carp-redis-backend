package db

import (
	"fmt"

	"github.com/redis/go-redis/v9"
)

func NewClient(host string, port, database int, username, password string) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", host, port),
		Username: username,
		Password: password,
		DB:       database,
	})

	return client
}
