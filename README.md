# CARP Redis Backend

The plugin will listen to cache start events, modifying the 
response to be returned to the client, using a Redis database 
instance as the storage method

## 🚀 Get Started

 Go 1.19 is required

1. Clone the repository

2. Exec in the root:

```bash
go run ./cmd/main/main.go
```

## ⭐ How to use

Use `CacheEnd` for save a cache given a `CacheResponse` specified in the [proto](https://gitlab.com/carp-suite/plugins/carpdriver/-/blob/main/carp.proto)

Use `CacheStart` for get a cache given a `CacheRequest` specified in the [proto](https://gitlab.com/carp-suite/plugins/carpdriver/-/blob/main/carp.proto)

## 🐞 Testing

```bash
go test ./.../
```